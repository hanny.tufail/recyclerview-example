package hanny.com.recyclerviewexample

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import hanny.com.recyclerviewexample.Extensions.inflate
import hanny.com.recyclerviewexample.Modals.Photo
import kotlinx.android.synthetic.main.recyclerview_item_row.view.*

class RecyclerAdapter(private val photos:ArrayList<Photo>):RecyclerView.Adapter<RecyclerAdapter.PhotoHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerAdapter.PhotoHolder {
        val inflatedView = parent.inflate(R.layout.recyclerview_item_row, false)
        return PhotoHolder(inflatedView)
    }

    override fun getItemCount(): Int = photos.size

    override fun onBindViewHolder(holder: RecyclerAdapter.PhotoHolder, position: Int) {
        val itemPhoto = photos[position]
        holder.bindPhoto(itemPhoto)
    }

    class PhotoHolder(itemView: View): RecyclerView.ViewHolder(itemView), View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        private var view: View = itemView //
        private var photo: Photo? = null

        companion object {
            private val PHOTO_KEY = "PHOTO"
        }

        override fun onClick(p0: View?) {
//            Log.d("RECYCLERVIEW","CLICKED")
            val context = itemView.context
            val detailsIntent = Intent(context, DetailsActivity::class.java)
            detailsIntent.putExtra(PHOTO_KEY, photo)
            context.startActivity(detailsIntent)
        }

        fun bindPhoto(photo: Photo) {
            this.photo = photo
            Picasso.with(view.context).load(photo.url).into(view.itemImage)
            view.itemDate.text = photo.humanDate
            view.itemDescription.text = photo.explanation
        }

    }
}